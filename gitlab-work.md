# GitLabの運用手順

## 1. Issueの作成
作成者は誰でも。

IssueのTitleを簡潔に書き、Descriptionには具体的に作業内容を書きます。
Assigneeは作業の担当者を選択します。

<img src="./img/make_issue.png" width=800 alt="Issue作成画面"></img>


## 2. Merge Requestの作成
実施者はIssueにアサインされた人。

IssueがアサインされるとTo-Do Listにタスクが割り当てられます。

<img src="./img/to_do_list.png" width=800 alt="To-Do List画面"></img>

"Cretae merge request"を選択してマージリクエストを作成します。
"Assignee"にマージを実行する担当者（吉田）、"Reviewer"にレビュー実施者（プラビンさん、アニルさん）を
指定しマージリクエストを作成します。この時点ではまだマージ出来ない状態です。

<img src="./img/make_mergereq.png" width=800 alt="Merge Request作成画面"></img>


## 3. ソースの変更
実施者はMerge Requestを作成した人 = Issueにアサインされた人。

ソースの変更作業と動作確認をMerge Requestで作成したブランチを使って行います。

**developブランチやmainブランチで行わないこと。**
**必ずMerge Requestで作成したブランチを使って変更作業を行ってください**


## 4. Merge Requestの発行
実施者はMerge Requestを作成した人 = Issueにアサインされた人。

変更作業が完了したらMerge Requestの"Mark as ready"を押して変更作業が完了したことを通知します。

<img src="./img/mark_as_ready.png" width=800 alt="Mark as ready画面"></img>


## 5. レビューの実施
実施者はMerge RequestでReviewerにアサインされた人。

ReviewerがアサインされるとTo-Do Listにタスクが割り当てられます。

Merge Requestのレビュー機能を使ってレビューします。
右上の"All threads resolved"になればレビュー指摘はすべて完了です。

<img src="./img/review_image.png" width=800 alt="Review画面"></img>


## 6. Mergeの実施
実施者はMerge RequestでAssigneeにアサインされた人。

AssigneeがアサインされるとTo-Do Listにタスクが割り当てられます。

変更内容、マージ元とマージ先を確認、他にもコンフリクトが発生しないか確認して、
"Merge"ボタンでマージを実施します。

<img src="./img/merge_image.png" width=800 alt="Merge画面"></img>

# Git使い方マニュアル

# 内容
[運用に使うコマンドだけに限定したGit簡易使い方マニュアル](https://gitlab.com/kanameg/git-beginner/-/blob/develop/git-basic-manual.md)

[GitLabの運用方法](https://gitlab.com/kanameg/git-beginner/-/blob/develop/gitlab-work.md)


# レビジョン
0.2
